function login(username, password, role){
	if(username === "" || password === "" || role === ""){
		console.log("Inputs must not be empty");
	}
	else{
		switch (role){
			case 'admin':
				console.log("Welcome back to the class portal, admin!");
				break;
			case 'teacher':
				console.log("Thank you for logging in, teacher!");
				break;
			case 'student':
				console.log("Welcome to the class portal, student!");
				break;
			default:
				console.log("Role out of range");
				break;
		}
	}
}

let username = prompt("Enter username");
let password = prompt("Enter password");
let role = prompt("Enter role");

login(username, password, role);



// 2. Check average

function checkAverage(num1, num2, num3, num4){
	let average = (num1 + num2 + num3 + num4) / 4;
	average = Math.round(average);
	console.log("Hello, student, your average is "  + average + "." + " The letter equivalent is ");

	if(average <= 74){
		console.log("F");
	} else if(average >= 75 && average <= 79){
		console.log("D");
	} else if(average >= 80 && average <= 84){
		console.log("C");
	} else if(average >= 85 && average <= 89){
		console.log("B");
	} else if(average >= 90 && average <= 95){
		console.log("A");
	} else if(average >= 96){
		console.log("A+");
	} 
}

checkAverage(95, 96, 97, 96);
