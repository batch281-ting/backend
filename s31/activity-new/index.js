const http = require('http');
const port = 3000;

const app = http.createServer((request, response) => {
	
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('You are in the login page');
	}
	else if(request.url == '/register'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("I'm sorry the page you are looking for cannot be found.");
	}
	else{
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('Page not available');
	}
});

app.listen(port);

console.log('Server is running at localhost: 3000');