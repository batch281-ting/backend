const User = require("../models/User");
const Course = require("../models/Course")
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The find method returns a record if a match is found
		if(result.length > 0){
			return true;

		// No duplicate emails found
		// The user is not registered in the database
		} else {
			return false;
		};
	});
};

// User registration
module.exports.registerUser = (reqBody) => {

	// Creates a variable named "newUser" and insantiates a new "User" object using the Mondoose model
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		// User registration failed
		if(error){
			return false;

		// User registration successful
		} else {
			return true;
		}
	})

};

// User authentication
module.exports.loginUser = (reqBody) => {
	// the findOne method returns the first record in the collection that matches the search criteria
	// We use findOne method instead of find method which returns all records that match the criteria
	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result == null){
			return false

		// User exists
		} else {
			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			// If the password matches
			if(isPasswordCorrect){
				// Generate an access token
				// Uses the createAccessToken method defined in the auth.js file
				return { access: auth.createAccessToken(result)}

			// Passwords do not match
			} else {
				return false;
			}
		}
	})
};

// Retrieve user details
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend
*/
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

// Enroll user to a class
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {

	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back
	let isUserUpdated = await User.findById(data.userData.id).then(user => {

		// Adds the courseId in the users enrollment array
		user.enrollments.push({courseId : data.courseId});

		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if(error){
				return false
			}else {
				return true;
			}
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.courseId});

		return course.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});

	});

	if(isUserUpdated && isCourseUpdated){
		return true;
	// User enrollment failure
	} else {
		return false
	};
}