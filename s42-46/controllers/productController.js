const Product = require("../models/Product");

const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.addProduct = (data) => {
  let product = new Product({
    imageURL: data.product.imageURL,
    name: data.product.name,
    description: data.product.description,
    price: data.product.price,
    isActive: data.product.isActive,
  });

  return product
    .save()
    .then(() => {
      return 'Product added successfully.';
    })
    .catch((error) => {
      console.error(error);
      throw new Error('Failed to add product.');
    });
};

// Retrieve all Products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

// Retrieve all active products
module.exports.getAllActive = (filterOption) => {
  let filter = {};
  if (filterOption === 'active') {
    filter.isActive = true;
  }
  return Product.find(filter).then((result) => {
    return result;
  });
};

// Retrieving a specific product
module.exports.getProduct = (productId) => {
  return Product.findById(productId)
    .then(result => {
      if (result) {
        return result;
      } else {
        throw new Error('Product not found');
      }
    })
    .catch(error => {
      console.error(error);
      throw new Error('Error retrieving product');
    });
};


// Update a product
module.exports.updateProduct = (productId, reqBody) => {

	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {

		
		if(error){
			return ('Failed to update product');

		
		} else {
			return ('Product updated successfully');
		}
	})

};

// archive a product
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		// Product not archived
		if (error) {

			return ('Failed to archive product');

		// Product archived successfully
		} else {

			return ('Product archived successfully');

		}

	});
};





// Activate a product

module.exports.activateProduct = (productId) => {
  return Product.findByIdAndUpdate(productId, { isActive: true }).then((product) => {
    if (product) {
      return { success: true };
    } else {
      return { success: false };
    }
  });
};


module.exports.deleteProduct = (req, res) => {
  const { productId } = req.params;

  Product.findByIdAndDelete(productId)
    .then(() => {
      res.status(200).json({ success: true, message: 'Product deleted successfully' });
    })
    .catch((error) => {
      console.error(error);
      res.status(500).json({ success: false, message: 'Failed to delete product' });
    });
};