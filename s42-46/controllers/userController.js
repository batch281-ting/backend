const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Order = require("../models/Order");


module.exports.checkEmailExists = (reqBody) => {
  return User.find({email : reqBody.email}).then(result => {
    // The find method returns a record if a match is found
    if(result.length > 0){
      return true;

    // No duplicate emails found
    // The user is not registered in the database
    } else {
      return false;
    };
  });
};

// User registration
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10),
    
  });

  return newUser.save()
    .then((user, error) => {
      // User registration successful
      if(error){
      return false;
    } else {
     return true;
    }
    })
};

// User authentication
module.exports.loginUser = (reqBody) => {
	
	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result == null){
			return false;

		// User exists
		} else {
	
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}

			// Passwords do not match
			} else {
				return false;
			}
		}
	})
};



// Get user profile with ordered products
module.exports.getProfile = async (data) => {
  try {
    const user = await User.findById(data.userId).select('_id email isAdmin');

    if (!user) {
      return { success: false, message: 'User not found' };
    }

    const orders = await Order.find({ userId: user._id });

    const profile = {
      user: {
        _id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
      },
      orders,
    };

    return { success: true, profile };
  } catch (error) {
    console.error(error);
    return { success: false, message: 'Internal Server Error' };
  }
};


// Set users admin
module.exports.setAsAdmin = (reqParams) => {
  const { userId } = reqParams;

  // Check if the user is already an admin
  return User.findById(userId)
    .then((user) => {
      if (!user) {
        return 'User not found';
      }

      if (user.isAdmin) {
        return 'User is already an admin';
      }

      // Update the user as admin
      user.isAdmin = true;
      return user.save().then(() => 'Updated Admin Status');
    })
    .catch((error) => {
      console.error(error);
      throw new Error('Internal Server Error');
    });
};


// Add to Cart
module.exports.addToCart = async (req, res) => {
  try {
    const userId = req.user.id;
    const { productId, name, description, quantity, price, subtotal } = req.body;

    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ error: 'User not found.' });
    }

    user.cart.items.push({
      productId, // Include the productId
      name,
      description,
      quantity,
      price,
      subtotal,
    });

    user.cart.totalAmount += parseFloat(subtotal);

    await user.save();

    res.status(200).json({ message: 'Item added to cart successfully.' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Failed to add item to cart.' });
  }
};


// Change product quantities
module.exports.updateUserCart = (data) => {
  return User.findOne({ _id: data.userId }).then(async (user) => {
    if (!user || !Array.isArray(user.cart.items)) {
      return `Your cart is empty. There is no item to update quantity.`;
    }

    let productExists = false;
    for (const prod of user.cart.items) {
      if (data.product && data.product.productId && data.product.productId === prod.productId) {
        prod.quantity = data.product.quantity;
        user.save();
        productExists = true;
        break;
      }
    }

    if (productExists) {
      return `Product with ID: ${data.product.productId} updated quantity to ${data.product.quantity}.`;
    } else {
      return `Product with ID: ${data.product.productId} does not exist in cart.`;
    }
  });
};

// Remove products from cart





// Route for subtotal for each item
module.exports.getCartSubtotals = async (req, res) => {
  try {
    const userId = req.user.id;
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json('User not found.');
    }

    const cartSubtotals = user.cart.items.map((item) => ({
      productId: item.productId,
      quantity: item.quantity,
      subtotal: item.subtotal,
    }));

    const subtotal = cartSubtotals.reduce((total, item) => total + parseFloat(item.subtotal), 0).toFixed(2);

    res.status(200).json({
      success: true,
      cartSubtotals,
      totalSubtotal: parseFloat(subtotal),
    });
  } catch (error) {
    console.error(error);
    res.status(500).json('Failed to retrieve cart subtotals.');
  }
};



// Total price for all items

module.exports.getTotalPrice = async (req, res) => {
  try {
    const userId = req.user.id;
    const user = await User.findById(userId);

    // Calculate the total price for all items in the cart
    const totalPrice = user.cart.items.reduce((total, item) => total + parseFloat(item.subtotal), 0).toFixed(2);

    res.status(200).json({
      success: true,
      totalPrice: parseFloat(totalPrice),
    });
  } catch (error) {
    console.error(error);
    res.status(500).json('Failed to calculate total price.');
  }
};

// Remove all

module.exports.removeAllItems = async (req, res) => {
  try {
    const userId = req.user.id;
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json('User not found.');
    }

    user.cart.items = []; // Remove all items/orders from the cart
    user.cart.totalAmount = 0; // Reset the total amount

    await user.save();

    res.status(200).json('All orders removed from the cart.');
  } catch (error) {
    console.error(error);
    res.status(500).json('Failed to remove all orders from the cart.');
  }
};

module.exports.getUserCart = (userInfo) => {
  return User.findById(userInfo.id)
    .populate('cart.items.productId')
    .exec()
    .then((user) => {
      if (!user.cart || user.cart.items.length === 0) {
        return 'Your cart is empty. Check our products to add one to the cart.';
      }

      let total = 0;

      for (const item of user.cart.items) {
        const product = item.productId;

        if (!product || item.quantity > product.quantity) {
          item.quantity = product ? product.quantity : 0;
        }

        item.subtotal = item.quantity * (product ? product.price : 0);
        total += item.subtotal;
      }

      return user.save().then(() => {
        return {
          cartItems: user.cart.items,
          totalAmount: total,
        };
      });
    })
    .catch((error) => {
      console.error(error);
      return 'Failed to retrieve cart orders.';
    });
};

// delete

module.exports.removeItemFromCart = (data) => {
  return User.findById(data.userId).then(async user => {
    if(user.cart == ""){
      return `Cannot remove more Item. Your cart is Empty.`
    }
    for(const prod of user.cart){
      if(data.product.productId == prod.productId){
        user.cart = user.cart.filter(item => item !== prod)
        user.save()
        return `Product with ID: ${prod.productId} removed from cart.`
      }
      return `Product with ID: ${data.product.productId} does not exist in cart.`
    }
  })
}
