const Order = require("../models/Order");
const User = require("../models/User");
const auth = require("../auth");
const Product = require("../models/Product")




// Non-admin User Checkout (Create Order)
// module.exports.checkoutAndCreateOrder = async (req, userData) => {
//   const { products } = req.body; // Destructure from req.body

//   try {
//     // Check if the user exists
//     const user = await User.findById(userData.id);
//     if (!user) {
//       return ('User not found');
//     }

//     // Calculate the total amount based on the quantity and price of products
//     let totalAmount = 0;
//     for (const product of products) {
//       const { productId, quantity } = product;
//       // Retrieve the product price from your database based on the productId
//       const productData = await Product.findById(productId);
//       if (!productData) {
//         return (`Product with ID ${productId} not found`;
//       }
//       const productPrice = productData.price;
//       totalAmount += productPrice * quantity;
//     }

//     // Create a new order
//     const order = new Order({
//       userId: userData.id,
//       products,
//       totalAmount,
//       purchasedOn: new Date(),
//     });

//     const savedOrder = await order.save();

//     // Initialize the orderedProducts array if it's undefined
//     user.orderedProducts = user.orderedProducts || [];

//     // Add the order to the user's orderedProducts array
//     user.orderedProducts.push(savedOrder._id);
//     await user.save();

//     return { success: true, order: savedOrder };
//   } catch (error) {
//     console.error(error);
//     return { success: false, message: "Internal Server Error" };
//   }
// };

module.exports.checkoutAndCreateOrder = async (req, userData) => {
  try {
    const userId = userData.id;
    const user = await User.findById(userId).populate('cart.items.productId');

    if (!user) {
      return { success: false, message: 'User not found.' };
    }

    // Check if the user has any items in the cart
    if (user.cart.items.length === 0) {
      return { success: false, message: 'Your cart is empty.' };
    }

    // Create an array of products for the order
    const products = user.cart.items.map((item) => ({
      productId: item.productId,
      quantity: item.quantity,
    }));

    // Calculate the total amount for the order
    const totalAmount = user.cart.totalAmount;

    // Create the order
    const order = new Order({
      userId: user._id,
      products,
      totalAmount,
    });
    await order.save();

    // Clear the cart after successful checkout
    user.cart.items = [];
    user.cart.totalAmount = 0;
    await user.save();

    return { message: 'Checkout successful.', order: order._id };
  } catch (error) {
    console.error(error);
    return { success: false, message: 'Failed to process checkout.' };
  }
};


// Retrieve authenticated user's orders
// module.exports.getUserOrders = async (req, userData, res) => {
//   const userId = userData.id;

//   try {
//     // Check if the user exists
//     const user = await User.findById(userId);
//     if (!user) {
//       return res.status(404).json({ success: false, message: 'User not found' });
//     }

//     // Retrieve the user's orders
//     const orders = await Order.find({ userId });

//     return res.status(200).json({ success: true, orders });
//   } catch (error) {
//     console.error(error);
//     return res.status(500).json({ success: false, message: 'Internal Server Error' });
//   }
// };

// module.exports.retrieveCheckout = async (req, userData) => {
//   try {
//     const userId = userData.id;
//     const user = await User.findById(userId).populate('cart.items.productId');

//     if (!user) {
//       return { success: false, message: 'User not found.' };
//     }

//     // Check if the user has any items in the cart
//     if (user.cart.items.length === 0) {
//       return { success: false, message: 'Your cart is empty.' };
//     }

//     // Return the user's cart and total amount
//     return { success: true, cart: user.cart.items, totalAmount: user.cart.totalAmount };
//   } catch (error) {
//     console.error(error);
//     return { success: false, message: 'Failed to retrieve checkout.' };
//   }
// };

module.exports.getOrders = async (req, userData) => {
  try {
    const userId = userData.id;
    const orders = await Order.find({ userId }).populate('products.productId');

    return { success: true, orders };
  } catch (error) {
    console.error(error);
    return { success: false, message: 'Failed to retrieve orders.' };
  }
};

module.exports.retrieveOrders = async (req, userData) => {
  try {
    // Check if the user has admin access
    if (userData.isAdmin !== true) {
      return { success: false, message: 'Admin access required.' };
    }

    // Retrieve all orders
    const orders = await Order.find().populate('userId').populate('products.productId');

    return { success: true, orders };
  } catch (error) {
    console.error(error);
    return { success: false, message: 'Failed to retrieve orders.' };
  }
};