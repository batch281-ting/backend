const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

const app = express();

mongoose.connect("mongodb+srv://christianting0304:bDzXnz7jpvuJ3fXT@wdc028-course-booking.n7rwsbm.mongodb.net/E-Commerce_API?retryWrites=true&w=majority",


	{
		useNewUrlParser : true,
		useUnifiedTopology :true
	});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection Error"))
db.once('open', () => console.log('Now connected to MongoDB Atlas!'))

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);



app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})