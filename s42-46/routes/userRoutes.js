const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// Route for setting user as admin (Admin only)
router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {
  const userInfo = auth.decode(req.headers.authorization);

  if (userInfo.isAdmin) {
    userController
      .setAsAdmin(req.params)
      .then((resultFromController) => res.send(resultFromController))
      .catch((error) => {
        console.error(error);
        res.status(500).send('Internal Server Error');
      });
  } else {
    res.status(403).send('You need to be an Admin for this request.');
  }
});




// Route for Add to Cart

router.post("/cart/add", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  req.user = userData;
  userController.addToCart(req, res);
});


// Route for change product quantites
router.put('/change-quantity', (req, res) => {
  const userInfo = auth.decode(req.headers.authorization);

  const data = {
    userId: userInfo.id,
    product: req.body,
  };

  userController.updateUserCart(data)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => {
      console.error('Error updating cart quantity:', error);
      res.status(500).send('An error occurred while updating cart quantity.');
    });
});



// Route for remove products from cart

router.delete('/cart', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  req.user = userData;
  userController.removeFromCart(req, res);
});



// Route for subtotal for each item

router.get('/cart/subtotals', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
  	req.user = userData;
	userController.getCartSubtotals(req, res);
});


// Route for total price for all items

router.get('/cart/total-price', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	req.user = userData
	userController.getTotalPrice(req, res);
});	



router.delete('/cart/remove-all', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	req.user = userData
	userController.removeAllItems(req, res);
});

// Route for retrieving cart orders
router.get('/cart/orders', (req, res) => {
  const userInfo = auth.decode(req.headers.authorization);

  userController.getUserCart(userInfo).then(resultFromController => res.send(resultFromController))
});


router.delete('/cart/remove', (req, res) => {
	const userInfo = auth.decode(req.headers.authorization);

	const data = {
		userId: userInfo.id,
		product: req.body
	}
	userController.removeItemFromCart(data).then(resultFromController => res.send(resultFromController))
})
module.exports = router;