const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");



// Route for creating a product
router.post("/", auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };

  if (data.isAdmin == true) {
    productController
      .addProduct(data)
      .then((resultFromController) => res.json({ message: resultFromController }))
      .catch((error) => res.status(500).json({ error: 'Failed to add product.' }));
  } else {
    res.status(401).json({ error: 'Unauthorized User' });
  }
});

// Route for retrieving all the products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the active products
router.get("/", (req, res) => {
  const filterOption = req.query.filter || 'all';

  if (filterOption === 'id') {
    // Handle search by ID
    const productId = req.query.productId;
    Product.findById(productId)
      .then((product) => {
        if (product) {
          res.send(product);
        } else {
          res.status(404).json({ error: 'Product not found' });
        }
      })
      .catch((error) => {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
      });
  } else {
    // Handle filter option 'all' or 'active'
    productController.getAllActive(filterOption)
      .then((resultFromController) => res.send(resultFromController))
      .catch((error) => {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
      });
  }
});
// Route for retrieving a specific product

router.get("/:productId", (req, res) => {
  const productId = req.params.productId;
  productController.getProduct(productId)
    .then(resultFromController => {
      res.status(200).json(resultFromController);
    })
    .catch(error => {
      console.error('Error retrieving product:', error);
      res.status(404).json({ error: 'Product not found' });
    });
});

// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  if (data.isAdmin) {
    productController
      .updateProduct(req.params.productId, data.product)
      .then((resultFromController) => res.json(resultFromController))
      .catch((error) => res.json({ success: false, message: 'Failed to update product' }));
  } else {
    res.status(401).send("Unauthorized User");
  }
});



// Route to archiving a product
router.patch("/:productId/archive", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;

  if (isAdmin) {
    productController.archiveProduct(req.params)
      .then(resultFromController => res.json({ message: resultFromController }))
      .catch(error => res.status(500).json({ error: 'Failed to archive product' }));
  } else {
    res.status(403).json({ error: 'Unauthorized User' });
  }
});






// Route tp activate product
router.put("/:productId/activate", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;

  if (isAdmin) {
    productController
      .activateProduct(req.params.productId)
      .then((resultFromController) => {
        if (resultFromController.success) {
          res.json({ success: true, message: 'Product activated successfully' });
        } else {
          res.json({ success: false, message: 'Failed to activate product' });
        }
      })
      .catch((error) => res.json({ success: false, message: 'Failed to activate product' }));
  } else {
    res.status(401).send("Unauthorized User");
  }
});

// DELETE /products/:productId
router.delete('/:productId', auth.verify, productController.deleteProduct);



module.exports = router;