const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");


// Route for non-admin user checkout (Create Order)
router.post("/checkout", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  orderController
    .checkoutAndCreateOrder(req, userData) // Pass req and userData as arguments
    .then((resultFromController) => {
      res.send(resultFromController);
    })
    .catch((error) => {
      console.error(error);
      res.status(500).send({ success: false, message: "Internal Server Error" });
    });
});


// Route for retrieve authenticated user's orders

router.get('/orders', auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    orderController
      .getOrders(req, userData)
      .then((result) => {
        res.json(result);
      })
      .catch((error) => {
        console.error(error);
        res.status(500).json({ success: false, message: 'Internal Server Error' });
      });
  } catch (error) {
    console.error(error);
    res.status(401).json({ success: false, message: 'Unauthorized' });
  }
});

// Route for retrieve all orders (Admin only)

// router.get('/allorders', auth.verify, (req, res) => {

// 	const data = {
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}

// 	if(data.isAdmin == true){
// 		orderController.getOrders(req.params).then(resultFromController => res.send(resultFromController));
// 	} else {
// 		res.send("Unauthorized User");
// 	}
// });

router.get('/allorders', auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    
    // Check if the user has admin access
    if (userData.isAdmin !== true) {
      return res.status(403).json({ success: false, message: 'Admin access required.' });
    }

    orderController
      .retrieveOrders(req, userData)
      .then((result) => {
        res.json(result);
      })
      .catch((error) => {
        console.error(error);
        res.status(500).json({ success: false, message: 'Internal Server Error' });
      });
  } catch (error) {
    console.error(error);
    res.status(401).json({ success: false, message: 'Unauthorized' });
  }
});


module.exports = router;
