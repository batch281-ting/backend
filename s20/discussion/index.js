let count = 5;

while(count !== 0){
	console.log("While: " + count);
	count--;
}

let count1 = 1;

while(count1 <= 5){
	console.log("While: " + count1);
	count1++;
}

/*
let number = Number(prompt("Give me a number: "));

do{
	console.log("Do while: " + number);

	number += 1;

} while(number <= 10);*/

let even = 2; 

do{
	console.log(even);
	even +=2;
}while(even <= 10);


for(let count = 0; count <= 20; count++){
	console.log(count);
}

let myString = "alex";

console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

console.log("Looping through array index");
for(let x = 0; x < myString.length;x++){
	console.log(myString[x]);
}

let myName = "AlEx";

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}

for(let count = 0; count <= 20; count++){
	if(count % 2 === 0){
		continue;
	}
	console.log("Continue and Break " + count);
	if(count > 10){
		break;
	}
}


let name = "alexandro";

for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}
	if(name[i] == "d"){
		break;
	}
}