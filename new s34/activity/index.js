const express = require('express');
const app = express();
const port = 3000;

app.get('/home', (req, res) => {
  res.send('Welcome to the home page');
});

const users = [
  {
    username: 'johndoe',
    password: 'johndoe1234',
  },
];

app.get('/users', (req, res) => {
  res.send(users);
});

// Delete user
app.use(express.json());

app.delete('/delete-user', (req, res) => {
  const { username } = req.body;
  let message;

  if (users.length > 0) {
    for (let i = 0; i < users.length; i++) {
      if (users[i].username === username) {
        users.splice(i, 1);
        message = `User ${username} has been deleted`;
        break;
      }
    }

    if (!message) {
      message = 'User does not exist';
    }
  } else {
    message = 'No users found';
  }

  res.send(message);
});

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
