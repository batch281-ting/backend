// JSON Object
/*
	-JSON stands for JavaScript Object Notation
	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

// JSON as objects
// {
// 	"city": "Quezon City",
// 	"provice": "Metro Manila",
// 	"country": "Philippines"
// }

// JSON Arrays

	// "cities": [
	// 	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	// 	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
	// 	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}	
	// ]

// Mini Activity - Create a JSON Array that will hold three breeds of dogs with properties: name, age, breed.

// "dogs": [
//   {"name": "Pocdol", "age": "1", "breed": "Golden Retriever"},
//   {"name": "Mio", "age": "2", "breed": "Labrador Retriever"},
//   {"name": "Wacky","age": "3", "breed": "German Shepherd"}
// ]

// JSON Methods
// Convert Data into Stringified JSON

let batchesArr = [{batchName: 'Batch X' }, {batchName: 'Batch Y'}];

// the "stringify" method is used to convert JavaScript objects into a string

console.log('Result from stringify method:');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
});

console.log(data); 

// Using Stringify method with variables
// User details
// let firstName = prompt('What is your first name?');
// let	lastName = prompt('What is your last name?');
// let age = prompt('What is your age?');
// let address = {
// 	city: prompt('Which city do you live in?'),
// 	country: prompt('Which country does your city address belong to?')
// };

// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// })

// console.log(otherData);

// Mini Activity - Create a JSON data that will accept user car details with variables brand, type, year.

// let brand = prompt('Enter the brand of the car:');
// let	type = prompt('Enter the type of the car:');
// let year = prompt('Enter the year of the car:');


// let carDetails = JSON.stringify({
// 	brand: brand,
// 	type: type,
// 	year: year
// })

// console.log(carDetails);

// Converting Stringified JSON into JavaScript Objects

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log('Result from parse method');
console.log(JSON.parse(batchesJSON));